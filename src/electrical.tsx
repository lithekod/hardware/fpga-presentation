import { Circle, CubicBezier, Line, Polygon, Rect, Txt, View2D } from "@motion-canvas/2d";
import { all, chain, createRef, createSignal, linear, Reference, Signal, SignalValue, SimpleVector2Signal, ThreadGenerator, Vector2, Vector2Signal } from "@motion-canvas/core";

export const offColor = '#444444'
export const badColor = '#fca614'
export const trueColor = '#15de17ff'
export const falseColor = '#de1570ff'

export const lineWidth = 7;

export const baseColors = [
  offColor,
  "#377eb8",
  "#4daf4a",
  "#984ea3",
];


export const east = new Vector2(10, 0);
export const west = new Vector2(-10, 0);

export interface Drawable {
  draw(view: View2D) : void
}

export interface Materializable {
  materialize(duration: number) : Generator<any, any>
}

export function *spawn(o: Drawable & Materializable, duration: number, view: View2D) {
  o.draw(view)
  yield* o.materialize(duration)
}

export function *spawnAll(objects: (Drawable & Materializable)[], duration: number, view: View2D) {
  yield* all(
    ...objects.map((o) => spawn(o, duration, view))
  )
}
export function *spawnChain(objects: (Drawable & Materializable)[], duration: number, view: View2D) {
  yield* chain(
    ...objects.map((o) => spawn(o, duration, view))
  )
}

export interface Trans {
  transition(fromColor: string, toColor: string, duration?: number): Generator<any, any>
  setColor(color: string): void
}

export class BaseOp {
  constructor(
    offset: SimpleVector2Signal<number>,
    position: Vector2,
    num_inputs: number,
    num_outputs: number,
    size?: Vector2,
  ) {
    this.basePosSig = offset;
    this.position = createSignal(position);
    this.fullPosition = createSignal(() => this.basePosSig().add(this.position()))
    this.num_inputs = num_inputs;
    this.num_outputs = num_outputs;
    this.size = structuredClone(BaseOp.baseSize)

    if (size) {
      this.size = size
    }
  }

  input_pos(idx: number): SimpleVector2Signal<number> {
    return createSignal(() => {
      const span = this.size.y - 70;
      let offset = new Vector2(this.size.x / 2., 0.);
      if (this.num_inputs > 1) {
        offset = new Vector2(
          this.size.x / 2,
          span / 2 - (span * (idx / (this.num_inputs - 1)))
        )
      }
      return this.fullPosition().sub(offset)
    })
  }

  output_pos(idx: number): SimpleVector2Signal<number> {
    return createSignal(() => {
      const span = this.size.y - 70;
      const offset = this.num_outputs == 1 ? new Vector2(
        this.size.x / 2,
        span / 2 + (span * (idx / (this.num_outputs - 1)))
      ) : new Vector2(this.size.x / 2., 0.);
      return this.fullPosition().add(offset)
    })
  }

  north(): SimpleVector2Signal<number> {
    return createSignal(() => {
      return this.fullPosition().sub(new Vector2(0, this.size.y / 2))
    })
  }
  south(): SimpleVector2Signal<number> {
    return createSignal(() => {
      return this.fullPosition().sub(new Vector2(0, -this.size.y / 2))
    })
  }


  basePosSig: SimpleVector2Signal<number> = createSignal(new Vector2(0, 0));

  op: string
  position: SimpleVector2Signal<number>
  readonly fullPosition: SimpleVector2Signal<number>
  num_inputs: number
  num_outputs: number
  fill: boolean = true

  topRef: Reference<Rect> = createRef()
  botRef: Reference<Rect> = createRef()
  public textRef: Reference<Txt> = createRef()

  static readonly baseSize = new Vector2(140, 140)
  size: Vector2
}


export class Operation extends BaseOp implements Trans, Drawable, Materializable {
  constructor(
    offset: SimpleVector2Signal<number>,
    op: string,
    position: Vector2,
    num_inputs: number,
    num_outputs: number,
    no_fill?: boolean,
    size?: Vector2,
  ) {
    super(offset, position, num_inputs, num_outputs, size)
    this.op = op

    if (no_fill) {
      this.fill = false
    }
    else {
      this.fill = true
    }
  }

  draw(view: View2D) {
    view.add(
      <Rect
        ref={this.botRef}
        fill={this.fill ? "#ffffff00" : null}
        stroke={this.fill ? "#ffffff" : null}
        lineWidth={lineWidth}
        position={this.fullPosition}
        width={this.size.x}
        height={this.size.y}
        radius={10}
      />)
    view.add(
      <Rect
        ref={this.topRef}
        fill={this.fill ? baseColors[1] : null}
        position={this.fullPosition}
        width={0}
        height={this.size.y - lineWidth}
        radius={10}
      />)
    view.add(<Txt
      fill='#ffffff'
      ref={this.textRef}
      position={this.fullPosition}
      scale={2}
    >
      {this.op}
    </Txt>
    )
  }

  *materialize(duration: number) {
    const originalSize = this.size
    yield* all(
      this.botRef().size(0, 0).to(originalSize, duration),
      this.textRef().scale(0, 0).to(2, duration)
    )
  }

  *transition(fromColor: string, toColor: string, duration?: number) {
    let duration_ = duration ? duration : 0.75;
    yield* all(
      this.botRef().fill(fromColor, 0),
      this.topRef().fill(toColor, 0),
      this.topRef().position.x(
        createSignal(() => this.fullPosition().x - this.size.x / 2),
        0
      ).to(createSignal(() => this.fullPosition().x), duration_, linear),
      this.topRef().width(0, 0).to(this.size.x - lineWidth, duration_, linear)
    )
  }

  setColor(color: string) {
    this.botRef().fill(color)
    this.topRef().fill(color)
  }


  op: string
  fill: boolean = true

  topRef: Reference<Rect> = createRef()
  botRef: Reference<Rect> = createRef()
  public textRef: Reference<Txt> = createRef()
}

export class Mux extends BaseOp implements Drawable {
  constructor(
    offset: SimpleVector2Signal<number>,
    position: Vector2,
    num_inputs: number,
    num_outputs: number,
    size?: Vector2,
  ) {
    super(offset, position, num_inputs, num_outputs, size)
    this.size.x = 100;
    this.size.y = 200;
  }

  draw(view: View2D) {
    view.add(
      <Line
      lineWidth={lineWidth}
      stroke={"#ffffffff"}
      points={[
        createSignal(() => new Vector2(
          - this.size.x/2,
          + this.size.y/2 + Mux.angleOffset
        )),
        createSignal(() => new Vector2(
          + this.size.x/2,
          + (this.size.y/2 - Mux.angleOffset)
        )),
        createSignal(() => new Vector2(
          + this.size.x/2,
          - (this.size.y/2 - Mux.angleOffset)
        )),
        createSignal(() => new Vector2(
          - this.size.x/2,
          - this.size.y/2 - Mux.angleOffset
        )),
        createSignal(() => new Vector2(
          - this.size.x/2,
          + this.size.y/2 + Mux.angleOffset
        )),
      ]}
      position={this.fullPosition}
      ref={this.linesRef}
      />)
  }

  *materialize(duration: number) {
    yield* this.linesRef().scale(0, 0).to(1, duration)
  }

  linesRef: Reference<Line> = createRef()
  static readonly angleOffset: number = 25
}

export class Register implements Drawable {
  constructor(basePosSig: SimpleVector2Signal<number>, position: Vector2, no_fill?: boolean) {
    this.basePosSig = basePosSig
    this.position = createSignal(() => this.basePosSig().add(position))

    if (no_fill) {
      this.fill = false
    }
    else {
      this.fill = true
    }
  }


  draw(view: View2D, ref?: Reference<Rect>) {
    view.add(
      <Rect
        ref={this.botRef}
        fill={this.fill ? baseColors[0] : null}
        position={this.position}
        width={this.size.x}
        height={this.size.y}
        radius={0}
      />)
    view.add(
      <Rect
        ref={this.topRef}
        fill={this.fill ? baseColors[1] : null}
        position={this.position}
        width={0}
        height={this.size.y}
        radius={0}
      />)
    view.add(
      <Rect
        position={this.position}
        width={this.size.x}
        height={this.size.y}
        stroke={'#ffffff'}
        lineWidth={lineWidth}
      />
    )

    view.add(
      <Polygon
        sides={3}
        stroke={"#ffffff"}
        lineWidth={lineWidth}
        size={this.size.x}
        x={createSignal(() => this.position().x)}
        y={createSignal(() => this.position().y + this.size.y / 2 - this.size.x * (1 / 4))}
      />)

    view.add(
      <Txt
        ref={this.textRef}
        fill={"#fff"}
        scale={1.5}
      />
    )
  }

  input_pos(idx: number): SimpleVector2Signal<number> {
    return createSignal(() => {
      let offset = new Vector2(this.size.x / 2, 0.);
      return this.position().sub(offset)
    })
  }

  output_pos(idx: number): SimpleVector2Signal<number> {
    return createSignal(() => {
      let offset = new Vector2(this.size.x / 2, 0.);
      return this.position().add(offset)
    })
  }

  clock_pos(idx: number): SimpleVector2Signal<number> {
    return createSignal(() => {
      let offset = new Vector2(0, this.size.y / 2);
      return this.position().add(offset)
    })
  }

  *setValue(toColor: string, value: string) {
    this.textRef().text(value)
    this.botRef().fill(toColor)
  }

  setColor(color: string) {
    this.botRef().fill(color)
  }

  position: SimpleVector2Signal<number>
  fill: boolean = true

  botRef: Reference<Rect> = createRef()
  public textRef: Reference<Txt> = createRef()

  readonly size: Vector2 = new Vector2(90, 250)
  readonly x_ratio = 3;

  basePosSig: SimpleVector2Signal<number> = createSignal(new Vector2(0, 0));
}

export class Wire implements Trans, Drawable, Materializable {
  constructor(start: SimpleVector2Signal<number>, sdir: Vector2, end: SimpleVector2Signal<number>, edir: Vector2) {
    this.start = start;
    this.sdir = sdir
    this.end = end
    this.edir = edir
  }

  draw(view: View2D) {
    view.add(
      <CubicBezier
        ref={this.botRef}
        lineWidth={8}
        stroke={"#ffffff"}
        p0={this.start}
        p1={createSignal(() => this.start().add(this.sdir))}
        p2={createSignal(() => this.end().add(this.edir))}
        p3={this.end}
        endArrow
      />
    )
    view.add(
      <CubicBezier
        ref={this.topRef}
        lineWidth={8}
        stroke={'#e13238'}
        p0={this.start}
        p1={createSignal(() => this.start().add(this.sdir))}
        p2={createSignal(() => this.end().add(this.edir))}
        p3={this.end}
        endArrow
        end={0}
      />
    )
  }

  *transition(fromColor: string, toColor: string, duration?: number) {
    yield* all(
      this.botRef().stroke(fromColor, 0),
      this.topRef().stroke(toColor, 0),
      this.topRef().end(0, 0).to(1, duration ? duration : 0.75, linear),
      this.topRef().end(0, 0).to(1, duration ? duration : 0.75, linear),
    )
  }

  setColor(color: string) {
    this.topRef().stroke(color)
    this.botRef().stroke(color)
  }

  *highlight() {
    yield* all(
      this.botRef().lineWidth(8, 0).to(32, 0.5).to(8, 1),
      this.topRef().lineWidth(8, 0).to(32, 0.5).to(8, 1),
    )
  }

  *pulse(fromColor: string, toColor: string, duration: number) {
    yield* all(
      this.botRef().stroke(fromColor, 0).to(toColor, duration)
    )
  }

  *materialize(duration: number) {
    yield* this.botRef().end(0, 0).to(1, duration)
  }

  setOn() {
    this.botRef().stroke(trueColor)
  }
  setOff() {
    this.botRef().stroke(falseColor)
  }
  setWhite() {
    this.botRef().stroke("white")
  }

  // Disconnect the output from the position it is connected to, leaving it in that position
  // even if the block moves away
  disconnectOutput() {
    this.end.save()
  }
  connectInput(pos: SimpleVector2Signal<number>) {
    this.start = pos
    this.botRef().p0(pos)
    this.topRef().p0(pos)
  }
  connectOutput(pos: SimpleVector2Signal<number>) {
    this.end = pos
    this.botRef().p3(pos)
    this.topRef().p3(pos)
  }

  *connectOutputRedirect(pos: SimpleVector2Signal<number>, direction: Vector2, duration: number) {
    this.end.save()
    yield* all(
      this.end(pos, duration),
      this.topRef().p2(createSignal(() => pos().add(direction)), duration),
      this.botRef().p2(createSignal(() => pos().add(direction)), duration)
    )
  }

  start: SimpleVector2Signal<number>
  sdir: Vector2
  end: SimpleVector2Signal<number>
  edir: Vector2

  topRef: Reference<CubicBezier> = createRef()
  botRef: Reference<CubicBezier> = createRef()
}



export class ClockGen extends BaseOp {
  constructor(basePosSig: SimpleVector2Signal<number>, pos: Vector2) {
    super(basePosSig, pos, 1, 1, new Vector2(140, 140))
    this.basePosSig = basePosSig

    this.pointEnd = createSignal(() => this.fullPosition().add(new Vector2(0, -140 / 2).rotate(this.angle())))
  }

  draw(view: View2D) {
    view.add(
      <Circle
        ref={this.circleRef}
        size={140}
        fill={offColor}
        stroke="#ffffff"
        lineWidth={lineWidth}
        position={this.fullPosition}
      />
    )
    view.add(
      <Line
        points={[this.fullPosition, this.pointEnd]}
        lineWidth={lineWidth}
        stroke={"#ffffff"}
      />
    )
  }

  *spin(fromColor: string, toColor: string, duration: number) {
    yield* this.angle(0, 0)
    yield* all(
      this.angle(360, duration, linear),
      this.circleRef().fill(fromColor, 0).to(toColor, duration / 2)
    )
  }
  *startSpin(duration: number) {
    yield* this.angle(90, 0)
    yield* all(
      this.angle(360, duration, linear),
    )
  }

  circleRef: Reference<Circle> = createRef()
  basePosSig: SimpleVector2Signal<number>
  angle = createSignal(0)
  pointEnd: SimpleVector2Signal<number>
}

