import {makeProject} from '@motion-canvas/core';

import title from './scenes/title?scene';
import hardware_vs_software from './scenes/hardware_vs_software?scene';
import example from './scenes/example?scene';
import two_ops from './scenes/two_ops?scene';
import feedback from './scenes/feedback?scene';
import rgb_protocol from './scenes/rgb_protocol?scene';
import register_intro from './scenes/register_intro?scene';
import feedback_with_register from './scenes/feedback_with_register?scene';
import counter from './scenes/counter?scene';
import fmax from './scenes/fmax?scene';

export default makeProject({
  scenes: [example, two_ops, feedback, register_intro, feedback_with_register, counter, fmax],
});
