import { Circle, View2D } from "@motion-canvas/2d";
import { createRef, Reference } from "@motion-canvas/core";

export class DummyDelay {
  constructor () {}

  *add(view: View2D) {
    var dummyCircle: Reference<Circle> = createRef();
    view.add(<Circle ref={dummyCircle} fill="#000000"/>)

    yield* dummyCircle().x(0, 0).to(0, 0.1)
  }
}
