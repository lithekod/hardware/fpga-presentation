import { makeScene2D } from "@motion-canvas/2d";
import { all, beginSlide, chain, createSignal, SimpleVector2Signal, Vector2 } from "@motion-canvas/core";
import { badColor, baseColors, Operation, Wire } from "../electrical";

export default makeScene2D(function* (view: any) {
  let pos: SimpleVector2Signal<number> = createSignal(new Vector2(0, 0));

  const inA = new Operation(pos, "A", new Vector2(-500, -300), 1, 2, true)
  const outSum = new Operation(pos, "Σ", new Vector2(500, 0), 1, 2, true)
  const adder = new Operation(pos, "+", new Vector2(0, 0), 2, 2)

  const wireA = new Wire(inA.output_pos(0), new Vector2(200, 0), adder.input_pos(0), new Vector2(-200, 0))
  const wireLoop0 = new Wire(
    adder.output_pos(0),
    new Vector2(200, 0),
    createSignal(() => adder.fullPosition().add(new Vector2(0, 250))),
    new Vector2(300, 0)
  )
  const wireLoop1 = new Wire(
    createSignal(() => adder.fullPosition().add(new Vector2(0, 250))),
    new Vector2(-300, 0),
    adder.input_pos(1),
    new Vector2(-200, 0)
  )
  const wireC = new Wire(adder.output_pos(0), new Vector2(200, 0), outSum.input_pos(0), new Vector2(-200, 0))
  wireA.draw(view);
  wireLoop0.draw(view);
  wireLoop1.draw(view);
  wireC.draw(view);

  inA.draw(view);
  outSum.draw(view);
  adder.draw(view);

  yield* chain (
    beginSlide("inputs"),
    all(
      inA.textRef().scale(2, 0).to(4, 0.5).to(2.0, 1),
      outSum.textRef().scale(2, 0).to(4, 0.5).to(2.0, 1),
    ),
    all (
      wireLoop0.highlight(),
      wireLoop1.highlight(),
    )
  );


  const frames: Array<[string, string, [number, number]]> = [
    [baseColors[0], baseColors[1], [1, 1]],
    [baseColors[1], baseColors[2], [3, 4]],
    [baseColors[2], baseColors[3], [-2, 2]],
    [baseColors[3], baseColors[0], [4, 6]],
  ]

  yield* beginSlide("operation");
  for (let [i, [initial, to, [inVal, outVal]]] of frames.entries()) {
    yield* all(
      inA.textRef().text(inVal.toString(), 0),
      chain(
        wireA.transition(initial, to),
        beginSlide("Badness start" + i),
        adder.transition(initial, badColor),
      )
    )
    yield* chain(
      all(
        wireC.transition(frames[0][0], badColor),
        wireLoop0.transition(frames[0][0], badColor),
      ),
      wireLoop1.transition(frames[0][0], badColor)
    )
  }

  yield* beginSlide("end")
})
