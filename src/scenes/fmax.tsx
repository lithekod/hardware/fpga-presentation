import { makeScene2D, Rect, Txt } from "@motion-canvas/2d";
import { all, beginSlide, chain, createRef, createSignal, delay, Reference, SimpleVector2Signal, Vector2, waitFor } from "@motion-canvas/core";
import { badColor, baseColors, ClockGen, east, falseColor, Mux, offColor, Operation, Register, spawnAll, Trans, trueColor, west, Wire } from "../electrical";

export default makeScene2D(function*(view: any) {
  let pos: SimpleVector2Signal<number> = createSignal(new Vector2(0, 0));

  const reg = new Register(pos, new Vector2(200, 0))
  const logic1 = new Operation(pos, "?", new Vector2(0, 0), 1, 2);
  const clkgen = new ClockGen(pos, new Vector2(200, 350))

  const l1out = new Wire(logic1.output_pos(0), new Vector2(20, 0), reg.input_pos(0), new Vector2(-20, 0))
  const loopback1 = new Wire(reg.output_pos(0), new Vector2(200, 0), createSignal(new Vector2(0, -300)), new Vector2(400, 0));
  const loopback2 = new Wire(createSignal(new Vector2(0, -300)), new Vector2(-400, 0), logic1.input_pos(0), new Vector2(-400, 0));
  const clkWire = new Wire(clkgen.north(), new Vector2(0, 10), reg.clock_pos(0), new Vector2(0, -10));
  reg.draw(view)
  logic1.draw(view)
  clkgen.draw(view)
  l1out.draw(view)
  loopback1.draw(view)
  loopback2.draw(view)
  clkWire.draw(view)

  yield* beginSlide("1 logic")
  function *runLoop(loopables: Trans[], color: string, after?: Generator<any, any>, setColor=true) {
    if (setColor) {
      reg.setColor(color)
      for (let thing of loopables) {
        thing.setColor(badColor);
      }
    }
    yield* all(
      chain(all(clkWire.pulse(badColor, "#ffffff", 1.0), clkgen.spin(badColor, baseColors[0], 2)),  after ? after : waitFor(0)),
      chain(...loopables.map((thing) => thing.transition(badColor, color, 0.32)))
    )
  }

  for (let i = 0; i < 6; i++) {
    let color = baseColors[i % (baseColors.length - 1) + 1]
    yield* runLoop([loopback1, loopback2, logic1, l1out], color)
  }


  const logic2 = new Operation(pos, "?", new Vector2(-250, 0), 1, 2);
  const l2out = new Wire(logic2.output_pos(0), east, logic1.input_pos(0), west)

  yield* all(
    pos(new Vector2(100, 0), 0.75),
    loopback2.connectOutputRedirect(logic2.input_pos(1), new Vector2(-200, 0), 0.75)
  )
  yield* spawnAll([logic2, l2out], 0.75, view)

  yield* beginSlide("2 logics")
  for (let i = 0; i < 4; i++) {
    let color = baseColors[i % (baseColors.length - 1) + 1]
    yield* runLoop([loopback1, loopback2, logic2, l2out, logic1, l1out], color)
  }

  yield* beginSlide("3 logics")

  const logic3 = new Operation(pos, "?", new Vector2(-500, 0), 1, 2);
  const l3out = new Wire(logic3.output_pos(0), east, logic2.input_pos(0), west)

  yield* all(
    pos(new Vector2(200, 0), 0.75),
    loopback2.connectOutputRedirect(logic3.input_pos(1), new Vector2(-200, 0), 0.75)
  )
  yield* spawnAll([logic3, l3out], 0.75, view)
  yield*beginSlide("3logics run")

  function* badness() {
    reg.setColor(badColor)
    yield* runLoop(loopables, badColor, null, false)
    yield* runLoop(loopables, badColor, null, false)
  }
  let loopables = [loopback1, loopback2, logic3, l3out, logic2, l2out, logic1, l1out]
  yield* runLoop(
    loopables,
    baseColors[1],
    badness(),
  )

  yield* beginSlide("Rerun")

  for (let l of loopables) {
    l.setColor(baseColors[1])
  }
  reg.setColor(baseColors[1])

  yield* beginSlide("Rerun_")

  yield* runLoop(
    loopables,
    baseColors[1],
    badness(),
  )

})
