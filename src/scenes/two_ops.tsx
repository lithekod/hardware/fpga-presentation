import { makeScene2D } from "@motion-canvas/2d";
import { all, beginSlide, chain, createSignal, sequence, SimpleVector2Signal, Vector2 } from "@motion-canvas/core";
import { badColor, baseColors, offColor, Operation, Wire } from "../electrical";

export default makeScene2D(function* (view) {
  let pos: SimpleVector2Signal<number> = createSignal(new Vector2(0, 0));

  const inA = new Operation(pos, "A", new Vector2(-500, -300), 1, 2, true)
  const inB = new Operation(pos, "B", new Vector2(-500, 300), 1, 2, true)
  const outC = new Operation(pos, "C", new Vector2(500, -100), 1, 2, true)

  const mul = new Operation(pos, "*", new Vector2(-150, 100), 2, 2)
  const adder = new Operation(pos, "+", new Vector2(150, -100), 2, 2)
  adder.draw(view);
  mul.draw(view);

  const ops = [
    inA,
    inB,
    outC,
    adder
  ];

  for (let op of ops) {
    op.draw(view)
  }

  const wireA = new Wire(inA.output_pos(0), new Vector2(200, 0), mul.input_pos(0), new Vector2(-200, 0))
  wireA.draw(view);
  const wireB = new Wire(inB.output_pos(0), new Vector2(200, 0), mul.input_pos(1), new Vector2(-200, 0))
  wireB.draw(view);

  const wireMul = new Wire(mul.output_pos(0), new Vector2(200, 0), adder.input_pos(1), new Vector2(-200, 0))
  wireMul.draw(view);
  const wireAAdd = new Wire(inA.output_pos(0), new Vector2(200, 0), adder.input_pos(0), new Vector2(-200, 0))
  wireAAdd.draw(view);

  const wireC = new Wire(adder.output_pos(0), new Vector2(200, 0), outC.input_pos(0), new Vector2(-200, 0))
  wireC.draw(view);


  const colors = [
    [baseColors[0], baseColors[1]],
    [baseColors[1], baseColors[2]],
    [baseColors[2], baseColors[3]],
    [baseColors[3], baseColors[0]],
  ]


  yield* beginSlide("simulation")

  const frames: Array<[string, string, [number, number]]> = [
    [baseColors[0], baseColors[1], [1,  2]],
    [baseColors[1], baseColors[2], [3,  5]],
    [baseColors[2], baseColors[3], [-2, 4]],
    [baseColors[3], baseColors[0], [4, -1]],
  ]

  for (let [i, [initial, to, [valA, valB]]] of frames.entries()) {
    yield* chain(
      all(
        inA.textRef().fill(initial, 0).to(to, 0),
        inA.textRef().text(valA.toString(), 0),
        inB.textRef().fill(initial, 0).to(to, 0),
        inB.textRef().text(valB.toString(), 0),
      ),
      all(
        wireA.transition(initial, to),
        wireB.transition(initial, to),
        wireAAdd.transition(initial, to),
      ),
    );
    if (i == 0) {
      yield* beginSlide("problem_highlight" + i)
      yield* wireAAdd.highlight(),
      yield* wireMul.highlight()
      yield* beginSlide("propagation" + i)
    }
    yield* chain(
      all(
        adder.transition(initial, badColor),
        mul.transition(initial, to),
      ),
    )
    yield* chain(
      all(
        wireMul.transition(initial, to),
        wireC.transition(initial, badColor),
      ),
      outC.textRef().text("?", 0),
    )
    if (i == 0) {
      yield* beginSlide("problematic ouput" + i)
    }
    yield* chain(
      adder.transition(badColor, to),
      wireC.transition(badColor, to),
      outC.textRef().text((valA * valB + valA).toString(), 0),
      beginSlide("Let it run" + i)
    );
  }
});
