import { makeScene2D, Rect, Txt } from "@motion-canvas/2d";
import { all, beginSlide, chain, createRef, createSignal, delay, Reference, SimpleVector2Signal, Vector2, waitFor } from "@motion-canvas/core";
import { badColor, baseColors, ClockGen, falseColor, Mux, offColor, Operation, Register, trueColor, Wire } from "../electrical";

export default makeScene2D(function*(view: any) {
  const layoutRef: Reference<Rect> = createRef();
  view.add(<Rect layout direction={'column'} gap={60} ref={layoutRef}>
      <Txt
        fill="#ffffff"
        scale={2}
        >Lets make a clock</Txt>

      <Txt
        fill="#ffffff"
        scale={1.5}
      >Coutning seconds</Txt>
    </Rect>
  )
  yield* beginSlide("Start")
  yield* layoutRef().remove()

  // Start of actual content
  let pos: SimpleVector2Signal<number> = createSignal(new Vector2(0, 0));

  const clkPos = new Vector2(0, 300)
  const in1 = new Operation(pos, "1", new Vector2(-400, 0), 1, 2, true)
  const clk = new Operation(pos, "clk", clkPos, 1, 1, true)
  const counter = new Operation(pos, "Cntr", new Vector2(0, 0), 1, 2, false, new Vector2(250, 140))
  const outOut = new Operation(pos, "out", new Vector2(400, 0), 1, 2, true)
  const wireIn = new Wire(in1.output_pos(0), new Vector2(200, 0), counter.input_pos(0), new Vector2(-200, 0))
  const wireOut = new Wire(counter.output_pos(0), new Vector2(200, 0), outOut.input_pos(0), new Vector2(-200, 0))
  const wireClk = new Wire(clk.north(), new Vector2(0, -100), counter.south(), new Vector2(0, 100))

  counter.draw(view);
  in1.draw(view);
  clk.draw(view);
  outOut.draw(view);
  wireIn.draw(view);
  wireOut.draw(view)
  wireClk.draw(view)

  yield* beginSlide("StartClk")


  const clkGen = new ClockGen(pos, clkPos)
  clkGen.draw(view)
  wireClk.connectInput(clkGen.north())
  clk.textRef().remove()

  var value = 0
  outOut.textRef().text(value.toString())
  yield* clkGen.startSpin(0.75)
  for (let _ of [...Array(10).keys()]) {
    value = value + 1
    outOut.textRef().text(value.toString())
    counter.textRef().text(value.toString())
    yield* all(
      wireClk.pulse(badColor, offColor, 0.5),
      clkGen.spin(badColor, offColor, 1),
    )
  }

  yield* beginSlide("BitFaster")

  for (let _ of [...Array(50).keys()]) {
    value = value + 1
    outOut.textRef().text(value.toString())
    counter.textRef().text(value.toString())
    yield* all(
      wireClk.pulse(badColor, offColor, 0.1),
      clkGen.spin(badColor, offColor, 0.2),
    )
  }


  yield* beginSlide("AddMax")
  const maxPos = new Vector2(0, -300)
  const maxVal = new Operation(pos, "maxVal", maxPos, 1, 1, true)
  const wireMax = new Wire(maxVal.south(), new Vector2(0, -50), counter.north(), new Vector2(0, 50))
  maxVal.draw(view)
  wireMax.draw(view)

  const maxValVal = 4
  yield* beginSlide("setMax")
  yield* maxVal.textRef().text("maxVal", 0).to(maxValVal.toString(), 1)

  yield* beginSlide("startCounting")
  for (let _ of [...Array(30).keys()]) {
    value = value >= 4 ? 0 : value + 1
    outOut.textRef().text(value.toString())
    counter.textRef().text(value.toString())
    yield* all(
      wireClk.pulse(badColor, offColor, 0.1),
      clkGen.spin(badColor, offColor, 0.2),
    )
  }

  yield* beginSlide("makingPulseStart")

  yield* all(
    outOut.textRef().text("out", 0.5),
    maxVal.textRef().text("maxVal", 0.5)
  )
  yield* beginSlide("makingPulse")
  yield* pos(new Vector2(-100, 0), 1)
  wireOut.disconnectOutput()
  yield* outOut.position(new Vector2(400, 0), 0)
  yield* outOut.position(new Vector2(700, 0), 1)

  const comparator = new Operation(pos, "==", new Vector2(400, 0), 1, 2, false)
  const comparatorMaxWire = new Wire(maxVal.south(), new Vector2(0, 100), comparator.north(), new Vector2(0, -250))
  const comparatorOutputWire = new Wire(comparator.output_pos(0), new Vector2(50, 0), outOut.input_pos(0), new Vector2(-50, 0))

  wireOut.connectOutput(comparator.input_pos(0))

  comparator.draw(view)
  yield* comparator.materialize(0.75)
  comparatorMaxWire.draw(view)
  yield* comparatorMaxWire.materialize(0.80)
  comparatorOutputWire.draw(view)
  yield* comparatorOutputWire.materialize(0.80)

  yield* beginSlide("pulseGenerationDemo")


  for (let _ of [...Array(30).keys()]) {
    value = value >= maxValVal ? 0 : value + 1
    counter.textRef().text(value.toString())
    if (value == maxValVal) {
      outOut.textRef().text("1")
      outOut.textRef().fill(trueColor)
      comparatorOutputWire.setOn()
    }
    else {
      outOut.textRef().text("0")
      outOut.textRef().fill(falseColor)
      comparatorOutputWire.setOff()
    }
    yield* all(
      wireClk.pulse(badColor, offColor, 0.1),
      clkGen.spin(badColor, offColor, 0.2),
    )
  }


  yield* beginSlide("countingPulses")
  comparatorOutputWire.setWhite()
  outOut.textRef().fill("white")
  outOut.textRef().text("out")


  yield* pos(new Vector2(-500, 0), 1)
  yield* beginSlide("add mux");

  const counter2y = -200;
  const mux = new Mux(pos, new Vector2(700, counter2y), 2, 2)
  const const0 = new Operation(pos, "0", new Vector2(500, counter2y - 65), 1, 2, true)
  const const1 = new Operation(pos, "1", new Vector2(500, counter2y + 65), 1, 2, true)
  const const0Wire = new Wire(const0.output_pos(0), new Vector2(25, 0), mux.input_pos(0), new Vector2(-25, 0))
  const const1Wire = new Wire(const1.output_pos(0), new Vector2(25, 0), mux.input_pos(1), new Vector2(-25, 0))
  comparatorOutputWire.disconnectOutput()
  yield* outOut.position(new Vector2(1300, -200), 1)


  mux.draw(view)
  const0.draw(view)
  const1.draw(view)
  const0Wire.draw(view)
  const1Wire.draw(view)
  yield* all(
    mux.materialize(0.75),
    comparatorOutputWire.connectOutputRedirect(mux.south(), new Vector2(0, 200), 1),
    const0.materialize(0.75),
    const1.materialize(0.75),
    const0Wire.materialize(0.75),
    const1Wire.materialize(0.75),
  )

  yield* beginSlide("muxAdded");

  const counter2 = new Operation(pos, "Cntr", new Vector2(1000, -200), 1, 2, false, new Vector2(250, 140))
  const counter2in = new Wire(mux.output_pos(0), new Vector2(25, 0), counter2.input_pos(0), new Vector2(-25, 0))
  const counter2out = new Wire(counter2.output_pos(0), new Vector2(25, 0), outOut.input_pos(0), new Vector2(-25, 0))
  const counter2clk = new Wire(clkGen.north(), new Vector2(0, -100), counter2.south(), new Vector2(0, 300))
  counter2.draw(view)
  counter2in.draw(view)
  counter2out.draw(view)
  yield* all(
    counter2.materialize(0.75),
    counter2in.materialize(0.75),
    counter2out.materialize(0.75),
  )

  yield* beginSlide("counterAdded");

  counter2clk.draw(view)
  wireClk.setWhite()
  yield* all(
    counter2clk.materialize(0.75),
    clkGen.position(new Vector2(400, 300), 0.75)
  )

  yield* beginSlide("clockConnected");


  var fullValue = 0
  for (let _ of [...Array(30).keys()]) {
    let atMax = value >= maxValVal
    value = atMax ? 0 : value + 1
    fullValue = atMax ? fullValue + 1 : fullValue
    counter.textRef().text(value.toString())
    counter2.textRef().text(fullValue.toString())
    outOut.textRef().text(fullValue.toString())
    if (value == maxValVal) {
      comparatorOutputWire.setOn()
    }
    else {
      comparatorOutputWire.setOff()
    }
    yield* all(
      wireClk.pulse(badColor, offColor, 0.1),
      counter2clk.pulse(badColor, offColor, 0.1),
      clkGen.spin(badColor, offColor, 0.2),
    )
  }

  yield* beginSlide("end")
})
