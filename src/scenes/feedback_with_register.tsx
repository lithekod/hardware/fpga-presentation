import { makeScene2D, Rect, Txt } from "@motion-canvas/2d";
import { all, beginSlide, chain, createRef, createSignal, delay, Reference, SimpleVector2Signal, Vector2, waitFor } from "@motion-canvas/core";
import { badColor, baseColors, Operation, Register, Wire } from "../electrical";

export default makeScene2D(function*(view: any) {
  let pos: SimpleVector2Signal<number> = createSignal(new Vector2(0, 0));

  var reg = new Register(pos, new Vector2(0, 0))
  const inA = new Operation(pos, "A", new Vector2(-700, 100), 1, 2, true)
  const adder = new Operation(pos, "+", new Vector2(-300, 0), 2, 2)
  const inClk = new Operation(pos, "clk", new Vector2(0, 350), 1, 2, true)
  const outOut = new Operation(pos, "Σ", new Vector2(500, 0), 1, 2, true)

  const wireIn = new Wire(inA.output_pos(0), new Vector2(200, 0), adder.input_pos(1), new Vector2(-200, 0))
  const wireOut = new Wire(reg.output_pos(0), new Vector2(200, 0), outOut.input_pos(0), new Vector2(-200, 0))
  const wireClk = new Wire(inClk.north(), new Vector2(0, -100), reg.clock_pos(0), new Vector2(0, 200))
  const wireRegAdder = new Wire(adder.output_pos(0), new Vector2(200, 0), reg.input_pos(0), new Vector2(-200, 0))


  const wireLoop0 = new Wire(reg.output_pos(0), new Vector2(200, 0), createSignal(() => adder.fullPosition().add(new Vector2(100, -250))), new Vector2(400, 0))
  const wireLoop1 = new Wire(createSignal(() => adder.fullPosition().add(new Vector2(100, -250))), new Vector2(-300, 0), adder.input_pos(0), new Vector2(-200, 0))


  function* triggerRegister(fromColor: string, toColor: string, toValue: string) {
    reg.textRef().text(toValue)
    reg.botRef().fill(toColor)
    yield* all(
      chain(
        wireOut.transition(fromColor, toColor),
        outOut.textRef().text(toValue, 0)
      ),
      chain(wireLoop0.transition(fromColor, toColor),
        wireLoop1.transition(fromColor, toColor)
      )
    )
  }
  function* updateAdder(fromColor: string, toColor: string) {
    yield* chain(
      adder.transition(fromColor, toColor),
      wireRegAdder.transition(fromColor, toColor)
    )
  }

  function* setA(fromColor: string, toColor: string, value: string) {
    inA.textRef().text(value)
    yield* wireIn.transition(fromColor, toColor)
  }

  function* risingClock() {
    inClk.textRef().text("1")
    yield* wireClk.transition(baseColors[0], badColor)
  }
  function* fallingClock() {
    inClk.textRef().text("0")
    yield* wireClk.transition(badColor, baseColors[0])
  }


  wireIn.draw(view)
  wireOut.draw(view)

  reg.draw(view)
  adder.draw(view)
  inA.draw(view)
  inClk.draw(view)
  outOut.draw(view)
  wireClk.draw(view)
  wireRegAdder.draw(view)
  wireLoop0.draw(view)
  wireLoop1.draw(view)

  reg.textRef().text("0")
  reg.botRef().fill(baseColors[1], 0)

  yield* beginSlide("start")

  yield* triggerRegister(baseColors[0], baseColors[1], "0")
  yield* updateAdder(baseColors[0], badColor)
  yield* beginSlide("setInput")

  yield* setA(baseColors[0], baseColors[1], "3")
  yield* updateAdder(badColor, baseColors[1])

  yield* beginSlide("waitTrigger")
  yield* risingClock()

  yield* triggerRegister(baseColors[1], baseColors[2], "3")
  yield* updateAdder(baseColors[1], badColor)
  yield* setA(baseColors[1], baseColors[2], "2")
  yield* updateAdder(badColor, baseColors[2])

  yield* beginSlide("waitTrigger2")
  yield* fallingClock()
  yield* risingClock()
  yield* triggerRegister(baseColors[2], baseColors[3], "5")
  yield* updateAdder(baseColors[2], badColor)

  yield* setA(baseColors[2], baseColors[3], "-2")
  yield* updateAdder(badColor, baseColors[3]),
  yield* fallingClock()

  yield* beginSlide("looping")


  const frames: Array<[string, string, [number, number]]> = [
    [baseColors[3], baseColors[1], [3,  4]],
    [baseColors[1], baseColors[2], [7,  1]],
    [baseColors[2], baseColors[3], [8, -2]],
    [baseColors[3], baseColors[1], [6, -2]],
  ]
  yield* risingClock();

  for (let [i, [initial, to, [valA, valB]]] of frames.entries()) {
    yield* chain(
      all(
        fallingClock(),
        triggerRegister(initial, to, valA.toString()),
        setA(initial, to, valB.toString()),
        updateAdder(to, badColor),
      ),
      all(
        chain(waitFor(1.0), risingClock()),
        updateAdder(badColor, to)
      )
    )
  }

  const rectRef: Reference<Rect> = createRef();
  const counterTextRef: Reference<Txt> = createRef();
  view.add(<Txt
    fill="#00000000"
    x={-400}
    y={-500}
    ref={counterTextRef}
    scale={1.5}
    >Cntr</Txt>)
  view.add(<Rect
    stroke="#ffffff00"
    width={900}
    height={550}
    x={-100}
    y={-40}
    lineWidth={5}
    ref={rectRef}
  />)

  yield* beginSlide("encapsulation")
  yield* rectRef().stroke("#ffffff00", 0).to("#ffffffff", 1);
  yield* rectRef().fill("#00000000", 0).to("#00000077", 1);
  yield* all(
    counterTextRef().fill("#ffff00", 0).to("#ffffffff", 1),
    counterTextRef().y(-280, 0).to(-380, 1)
  )
  yield* all(
    inA.textRef().text("-2", 0).to("inc", 1),
    inClk.textRef().text("1", 0).to("clk", 1),
    outOut.textRef().text("6", 0).to("out", 1)
  )

  yield* beginSlide("end")

})
