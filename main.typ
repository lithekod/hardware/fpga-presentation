#import "theme.typ": *
#import "highlight.typ": *
#import "@preview/diagraph:0.2.1": *

#set text(size: 25pt, font: "Noto Sans")

#show: university-theme.with(
  progress-bar: false,
  content-color: rgb("#1EC6BA"),
  color-b: rgb("#202020"),
  background-color: rgb("#202020"),
)

#show: highlights

// #enable-handout-mode(true)

#let cols(content1, content2) = {
  grid(columns: (50%, 50%), content1, content2)
}

#let slide-middle(..args, body) = {
  slide(..args, {
    v(1fr)
    body
    v(2fr)
  })
}

#title-slide(
  authors: ("Frans Skarman"),
  title: "FPGAs for Programmers",
  institution-name: "",
)

#slide()[
  #grid(columns: (50%, 50%), [
    #include "./animated_out/software_rust.typ"
  ],
  [
    #include "./animated_out/software_asm.typ"
  ])
]

#slide()[
  #set align(center)
  #only("1")[#image("./fig/chip_input_only.svg")]
  #only("2")[#image("./fig/chip_first_adder.svg")]
  #only("3")[#image("./fig/chip_second_adder.svg")]
  #only("4")[#image("./fig/chip_with_output.svg")]
]

#slide(title: [Why FPGA?])[
  #cols([
    #image("./fig/chip_with_output.svg", width:90%)
  ],
  [
    #line-by-line([
      - Everything works in parallel
      - The latency is known to the clock cycle
      - Easy interaction with external hardware
      - *It's fun!*
    ])
  ])
]

#slide()[
  #set align(center)
  #set text(size: 32pt)

  #v(3cm)
  Some Hardware Primitives
]

#slide(title: [Boolean Logic])[
  #only("1", [#image("./fig/logic_gates_base.svg", width:85%)])
  #only("2", [#image("./fig/logic_gates_murica.svg", width:85%)])
]


#slide(title: [Math])[
  #only("1", [#image("./fig/math_gates_base.svg", width:85%)])
  #only("2", [#image("./fig/math_gates_scary.svg", width:85%)])
  #only("3", [#image("./fig/math_gates_base.svg", width:85%)])
  #only("4", [#image("./fig/math_gates_with_division.svg", width:85%)])
]


#slide()[
  #set align(center)
  #only("1")[#image("./fig/mux_base.svg")]
  #only("2")[#image("./fig/mux_sel_a.svg")]
  #only("3")[#image("./fig/mux_sel_b.svg")]
]


#slide()[
  #set text(size:20pt)
  #cols([
    #include "./animated_out/alu.typ"
    ],
    [
      #set align(center)
      #only("5", image("./fig/alu_base.svg", width: 90%))
      #only("6", image("./fig/alu_separate_inputs.svg", width: 90%))
      #only("7", image("./fig/alu_sel_add.svg", width: 90%))
      #only("8", image("./fig/alu_sel_sub.svg", width: 90%))
      #only("9", image("./fig/alu_sel_mul.svg", width: 90%))
    ]
  )
]

#slide(title: [Recap so Far])[
  - Hardware is physical primitives inside a chip
  - Hardware description selects components and how to connect them
  - Programming looks similar
  - *But:* Everything is done in parallel
]

#slide()[
  #set align(center)
  #set text(size: 32pt)

  #v(3cm)
  Dealing with state
]

#slide()[
  #set align(center)
  #image("./fig/the_real_world.jpg")
]

#slide(title: [Counter])[
  #set text(size: 20pt)
  #include "./animated_out/accumulator.typ"
]

#slide(title: [Cascaded counters])[
  #set text(size: 20pt)
  #include "./animated_out/cascaded_counters.typ"
]


#slide(title: [Inlined])[
  #set text(size: 20pt)
  ```spade
  entity main(clk: clock, rst: bool) -> int<20> {
    let max = 4;
    reg(clk) fast_count reset(rst: 0) =
      if fast_count == max { 0 } else { trunc(fast_count + 1) };

    let tick = fast_count == max;

    reg(clk) seconds reset(rst: 0) =
      if tick {trunc(seconds + 1)} else {seconds};

    seconds
  }
  ```
]

#slide()[
  #uncover("1-", [We have a very limited programming model!])

  #image("./fig/programming_model.svg", height: 50%)

  #uncover("2-", [- Compute new value of all registers using current values])
  #uncover("3-", [- Update registers simultaneously])

  #uncover("4-", [No loops, no conditional execution etc.])
]

#slide()[
  #set align(center)
  #v(3cm)
  One more example:

  A dot moves along a line. Press a button when it is in the middle
]

#slide(title: [In Software])[
  #set text(size: 18pt)
  #cols(
    [#include "./animated_out/game.typ"],
    [
      #uncover("7-", [Loops kind of encode state])
      #uncover("8-", [- #box([#only("8", [*Menu*])#only("9-", [Menu])])])
      #uncover("8-", [- #box([#only("9", [*Game*])#only("10-", [Game])])])

      #uncover("10-", [And associated state])
    ]
  )
]

#slide(title: [In Hardware])[
  #set text(size: 18pt)
  #include "./animated_out/state_machine.typ"
]

#slide()[
  #set text(size: 32pt)
  #set align(center)

  #v(3cm)
  Compilation and Performance
]

#slide(title: [Basic Building Blocks])[
  #uncover("1-", [
    Look Up Tables (LUT)
    - Programmable to arbitrary $4 "bit" #sym.arrow 1 "bit"$ functions
    - Thousands per FPGA
  ])
  #uncover("2-", [
    Digital Signal Processing (DSP) blocks
    - Multiplier and Adder
    - Tens to hundreds per FPGA
  ])
  #uncover("3-", [
    Memories
    - Blocks of memory in configurable chunks
    - Kilobits to megabits per FPGA
  ])
]


#slide(title: [Compilation])[
  #grid(columns: (50%, 50%), [
    #v(4cm)
    ```spade
    fn add_three(a, b, c,) {
      a + b + c
    }
    ```
  ],
  [
    #image("./fig/128_bit_adder.svg")
  ])
]

#slide()[
  #cols([
    #image(width: 90%, "./fig/black_magic3.jpg")
  ],
  [
    #line-by-line([
      - Circuit Board
      - Blood
      - Sacrifice
        - Preferably a goat
        - Rabbit works in a pinch
      - Cat ears
    ])
  ])
]

#slide(title: [Synthesis])[
  #cols([
    ```spade
    fn add_three(a, b, c,) {
      a + b + c
    }
    ```
  ],
  [
    List of "nets"

    2 adders. A should connect to input 1 of adder 1...
  ])
]

#slide(title: [Place and Route])[
  #cols(
    [
      List of "nets"

      2 adders. A should connect to input 1 of adder 1...
    ],
    [
      Placement selects a *physical* location for each component

      Routing selects how to connect them
    ]
  )
]

#slide()[
  Software performance is simple

  #line-by-line([
    - Only 1 metric: runtime
    - Runtime degrades slowly
  ])
]

#slide(title: [Resource Usage])[
  #cols([
      #image("./fig/128_bit_adder.svg", height: 90%)
    ], [
      #line-by-line([
        - Each computation you perform takes some basic cells
        - Very binary transition from ok to bad
        - You still pay for unused components
      ])
    ])
]

#slide(title: [Clock Frequency])[
  #set align(center)
  #image("./fig/the_real_world.jpg")
]

#slide(title: [Clock Frequency])[
  #line-by-line([
    - (Mosty) Fixed frequency clock. $10-200 "MHz"$
    - *Static* timing analysis
    - Another pass/fail metric
  ])
]

#slide(title: [Performance])[
  #set text(size: 20pt)

  ```
  [INFO] Place and route maximum frequencies:
  clk$SB_IO_IN_$glb_clk: 30.5 MHz (target: 12 MHz) <- Is the design fast enough?
  [INFO] Place and route components:
  ICESTORM_LC: 181/1280 (14.1%) <- LUTs
  ICESTORM_PLL: 0/1      (0.0%)
  ICESTORM_RAM: 0/16     (0.0%)
  SB_GB: 2/8            (25.0%)
  SB_IO: 2/112           (1.8%)
  SB_WARMBOOT: 0/1       (0.0%)
  ```
]


#slide(title: [Performance])[
  #set text(size: 20pt)

  ```
  [INFO] Place and route maximum frequencies:
  $glbnet$_e_880[0]: 231.3 MHz (target: 200 MHz) <- Is the design fast enough?
  [INFO] Place and route components:
  ALU54B: 0/78              (0.0%)
  CLKDIVF: 0/4              (0.0%)
  DCCA: 6/56               (10.7%)
  DCSC: 0/2                 (0.0%)
  ...
  TRELLIS_COMB: 9193/83640 (11.0%) <- LUTs
  TRELLIS_ECLKBUF: 0/8      (0.0%)
  TRELLIS_FF: 3829/83640    (4.6%) <- Registers
  ```
]


#title-slide(
  authors: ("Gustav Sörnäs"),
  title: "WS2812b Addressable RGB LEDs",
  institution-name: "",
)

#slide-middle(title: [What is WS2812b?])[
  #line-by-line[

    - Addressable RGB LEDs
      - Individually controllable
    - Mounted on a PCB, or on a strip, or in a matrix, or flying free
    - Connected in series
    - Many names, same protocol
      - WS2811, 2812, 2812b, 2813
      - APA104, 106
      - SK6812
      - NeoPixel
  ]
]

#slide-middle(title: [What is it used for?])[
    Anytime bright lights are fun. (Meaning, wherever you want!)
    #v(0.5em)

  #line-by-line[

    - Keyboard backlight and underglow
    - Decoration: Taped to anything, anywhere
    - Matrix: text, graphics, clocks
  ]
]

#slide-middle(title: [What are its issues?])[
  #line-by-line[
  - Connected in series - one break is enough to stop transmission
  - Power delivery
  - Operating frequency
  ]
]

#slide-middle(title: [Why do we want it in hardware?])[
  800 KHz operating frequency means a challenge for both:

  #line-by-line[

    - Software implementation in a microcontroller (bit-banging)
      - Slow clock
      - Dependent on clock speed
    - General purpose CPUs (without a real-time operating system)
  ]
]

#slide-middle(title: [How does the protocol work?])[
  Recall that LEDs are connected serially.

  #image(width: 60%, "fig/cascade-leds.png")
]
#slide-middle[
  #grid(
    columns: (1fr, 1fr),
    [#v(1fr)Keyboard example:#v(2fr)],
    image(width: 100%, "fig/keyboard-leds.png")
  )
]

#slide-middle[
  Send a stream of colors. Every LED reads the first and sends the rest along.

  #align(center, image(width: 60%, "fig/led-serial.png"))
]

#slide-middle[
  Pixel data is sent as... _checks notes_

  #uncover("2-", )[
    #align(center, image("fig/bgr-data.png"))

    GRB???
  ]

]


#slide()[
  #align(center, image("./fig/the_real_world.jpg"))
]

#slide-middle[
  #grid(
    columns: (1fr, 1fr),
    [
      #v(1fr)
      Bits are different lengths of high+low, _not_ simple low/high once per clock cycle.

      Reset marks start of next data cycle. (RET is reset, not return.)
      #v(2fr)
    ],
    {
      v(1fr)
      image(width: 100%, "fig/bit-sequences.png")
      v(2fr)
    }
  )
]

#slide-middle(title: [Protocol recap])[
  #line-by-line[

    - 24 bits per LED
    - Bits are have a low pulse and high pulse, with different lengths signifying 0/1
    - Send LED data in the same order it should be on the strip
    - Reset between every transmission
  ]
]

#slide-middle(title: [How is the project setup?])[
  #image("fig/block-states.svg")
]
#slide-middle(title: [How is the project setup?])[
  #set text(size: 20pt)
  ```spade
  entity user_code(clk: clock, rst: bool, timing: Timing) -> bool {
    let state: OutputControl<int<4>> = inst state_gen(clk, rst, 3, timing);
    //                       ~~~~~~                             |
    //                                         we want 3 leds --+

    let with_color = match state {
      OutputControl::Ret => OutputControl::Ret,
      OutputControl::Led$(payload: led_num, bit, duration) => {
        let color = Color(0, 20, 0); // everything is green

        OutputControl::Led$(payload: color, bit, duration)
      }
    };

    output_gen(with_color, t)
  }
  ```
]

#slide-middle(title: [Available FPGAs])[
  #grid(
    columns: (1fr, 1fr, 1fr),
    uncover("2-")[
    - Go Board
      - iCE40 HX1K
      - 1 280 LUTs
      - 25 MHz clock
      - 1 PMOD (8 pins)
      - 4 buttons
      - UART interface
    ],
    uncover("3-")[
      - ECPIX-5
        - ECP5 45F
        - 45 000 LUTs
        - 100 MHz clock
        - 8 PMOD (64 pins)
        - External buttons
        - UART interface
    ],
    uncover("4-")[
      - ULX3S
        - ECP5 85F
        - 84 000 LUTs
        - 25 MHz clock
        - 28 pins
        - 6 buttons
        - UART interface
    ]
  )

  #uncover("5-")[
    Watch out for different clock speeds when dealing with counters.

    For now, choose one clock speed and stick to it.
  ]
]

#slide-middle(title: [Installing and running Spade])[
  Spade is the compiler, Swim is the build system.

  Swim takes care of the different FPGAs we have.
  #v(0.5em)

  #line-by-line[

    1. Install Swim: \ `cargo install --git https://gitlab.com/spade-lang/swim`
    2. Clone the WS2812 repository: \ `git clone https://gitlab.com/lithekod/hardware/fpga-ws2812`
    3. Change directory and build: \ `cd fpga-ws2812` \ `swim build`
  ]

  #uncover("5-")[`https://lithekod.se/hardware/fpga-evening`]
]

#slide-middle(title: [How do you get the code to the FPGA?])[
  #uncover("1-")[In a real project, `swim upload` runs synthesis, pnr, and upload.]

  #uncover("2-")[Today, me and Frans will keep the FPGAs by our computers, since we only have a few.]

  #uncover("3-")[
    Because I didn't think about this earlier, you will probably have to send the code to us using e.g. `https://paste.rs/web`. \ (Sorry!)

    Please ask if something is unclear.
  ]
]

#slide-middle(title: [Some things to do])[
  Roughly in increasing order of difficulty.
  #v(0.5em)

  #line-by-line[

  1. Change the color of all three LEDs to yellow.
  2. Control 10 LEDs.
  3. Have different colors for all LEDs. Hint: arrays.
  4. Animate in some way. For example, toggle on/off every second. Hint: counters, array offsets.
  5. React to pressing buttons on the FPGA. (You will need to make some more changes in the same file.)
  ]
]

#slide-middle(title: [Some more things to do])[
  Please tell us if you want to do any of these!
  #v(0.5em)

  #line-by-line[

  6. Control the FPGA from your computer.
    - All FPGAs can be communicated with via UART. We have a UART implementation in Spade somewhere on GitLab.
  7. Control the FPGA from a microcontroller.
    - We have Arduino Uno, ESP8266 and Raspberry Pi Pico (and hopefully enough level-shifters).
    - Hint: SPI.
  8. Use an RFID reader to draw a unique color pattern.
    - We only have one reader. Maybe do this one as a group.
  ]
]

#slide-middle(title: [Closing remarks])[]
