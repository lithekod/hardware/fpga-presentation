# NOTE: Does not compile the main project. `typst watch main.typ` should be used for that

all:
	echo "Don't run make all to compile the presentation. Use 'typst watch main.typ'"

DROMS=$(wildcard fig/wavedrom/*.json)
DROM_SVGS=$(patsubst fig/wavedrom/%.json, fig/%.svg, $(DROMS))

wavedrom: ${DROM_SVGS}

%.svg: wavedrom/%.json Makefile fig/dark.json5
	wavedrom -i $< -o $@ --skin fig/dark.json5


ANIMATED_SOURCES=$(wildcard code/animated/*)
ANIMATED_TARGETS=$(patsubst code/animated/%,animated_out/%.typ,${ANIMATED_SOURCES})

animated_out/%.typ: code/animated/% Makefile
	@mkdir ${@D} -p
	@echo "Building animated slides for $<"
	codepresenter $< -o animated_out typst ./animated_prelude.typ

animated_all: ${ANIMATED_TARGETS}
